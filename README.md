# Graph embedding semi-supervised learning project

## Introduction
Reimplementation of the Planetoid algorithm, according to the following paper:

[Revisiting Semi-Supervised Learning with Graph Embeddings](https://arxiv.org/abs/1603.08861),
Zhilin Yang, William W. Cohen, Ruslan Salakhutdinov. ICML 2016.

The project repository is https://bitbucket.org/fxz/graphemb

## Running the model

There is an example script for running the model `graphemb/test/trans_example.py` under the `graphemb/test` directory

To install all the requirements, run

```bash
pip install -r requirements.txt
```

To run the model, issue the following commands

```bash
cd graphemb/test
python trans_example.py --config citeseer.json
```

Refer to `example.json` for an idea of the config file.
In this repository, we have prepared `MNIST` and `CiteSeer` datasets for demonstration.
See the modules in the `dataset` directory for preparing other custom datasets.
