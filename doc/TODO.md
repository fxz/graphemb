# TODO list

### First steps
[ X ] Set up tensorflow and construct a basic feed-forward NN
[  ] Preprocess data (put them into the memory and in the desired data stucture)
[  ] Search for suitable dataset

### Network modeling
[  ] Build the real network that's the same as in the paper

### Graph construction
[  ] Construct a complete graph
[ X ] Make the in-memory graph data structure for use in the model
[  ] Add function argument to support user-defined norm
[  ] Make similarity matrix from networkx graph
[  ] Implement spectral embedding algorithm
### Sampling algorithm
### Integration

## Milestones
[ X ] Learn the structure of the neural network 
[  ] Prototype of each modules
[  ] One layer on each part, complete connection graph only, inductive working model
[  ] Benchmarking on MNIST
[  ] More sophisticated graph algorithm or embedding algorithm
