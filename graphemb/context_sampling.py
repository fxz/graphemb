import numpy as np

def ContextSampling(yLabels, Graph, r1, r2, q, d):
    '''
    FUNCTION INPUT
        Graph A: n*n matrix: a_ij is similarity between i and j instances, n = L + U
        Labels y: L*1 array: labels of the first L instances

    DEFINE PARAMETERS
        i: instance
        c: context
        gamma: +1 for positive pair (same label), -1 for negative pair (opposite label)
        r1: prob of positive pair
        r2: prob of graph-based sample; 1 - r2: prob of labeled sample
        q: random walk length
        d: window size
    '''

    (i, c, gamma) = (0, 0, 0)

    L = len(yLabels) # Number of labeled data
    U = Graph.shape[0] - L # Number of unlabeled data
    S = []
    ic_list = np.empty((0, 2))

    #With probability r1, assume (i, c) is a positive pair
    if np.random.rand() < r1:
        gamma = 1
    else:
        gamma = -1

    #With probability r2, generate random walk S in graph A of length q
    if np.random.rand() < r2:
        #print('random walk')
        S.append(np.random.randint(0,high = L + U))
        for S_iter in range(q):
            #Add S_i+1 by probability aij/Sum_j aij
            GraphSum = np.sum(Graph[S[S_iter]])
            S.append(np.random.choice(Graph.shape[0], p=Graph[S[S_iter]]/GraphSum))
        #Uniformly sample (S_j, S_k) with |j - k| < d

        for ti in range(q):
            for tc in range(q):
                if np.absolute(ti - tc) < d:
                    ic_list = np.append(ic_list, [[ti, tc]], axis=0)
        (i, c) = ic_list[np.random.randint(0, high=len(ic_list))]

        #if len(ic_list) == 0:
        #    print("No random walk sample within the window")

        #Uniformly corrupt c to sample a negative pair
        if gamma is -1:
            c = np.random.randint(0, high = L + U)

    else:
        yLabels_cat = list(set(yLabels))
        if len(yLabels_cat) is 1:
            print('Warning: All data have the same label.')
            gamma=1
        label_dict = {}
        for l in yLabels_cat:
            label_dict[l] = []
        for idx, l in enumerate(yLabels):
            label_dict[l].append(idx)

        #when gamma = 1, uniformly sample (i, c) with y_i = y_c
        if gamma is 1:
            #print('+L')
            while True:
                ic_label = np.random.choice(yLabels_cat, p=[len(label_dict[idx])/len(yLabels) for idx in yLabels_cat])
                if len(label_dict[ic_label]) > 1:
                    break
            (i, c) = np.random.choice(label_dict[ic_label], size=2)

        #when gamma = -1, uniformly sample (i, c) with y_i != y_c
        if gamma is -1:
            #print('-L')
            ic_label = np.random.choice(yLabels_cat, size=2, p=[len(label_dict[idx])/len(yLabels) for idx in yLabels_cat])
            i = np.random.choice(label_dict[ic_label[0]])
            c = np.random.choice(label_dict[ic_label[1]])

#    print(i, c, gamma)
    return np.array([i, c, gamma], dtype=np.int32)

def ContextBatch(yLabels, Graph, N2=200, r1=0.8, r2=0.8, q=10, d=3):
    while True:
        yield np.array([ContextSampling(yLabels, Graph, r1, r2, q, d) for _ in range(N2)])

def LabeledBatch(yLabels, N1):
    while True:
        shuffled_idx = np.random.permutation(len(yLabels))
        curr_idx = 0
        while curr_idx + N1 <= len(yLabels):
            curr_idx = curr_idx + N1
            yield shuffled_idx[curr_idx - N1:curr_idx]


