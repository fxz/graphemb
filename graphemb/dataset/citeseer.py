import sys
import _pickle as cPickle
import os

sys.path.append('..')

import graph

prefix = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                      'citeseer/trans.citeseer')

data = {}

data['x'] = cPickle.load(open(prefix+'.x', 'rb'), encoding='latin1').toarray()
data['y'] = cPickle.load(open(prefix+'.y', 'rb'), encoding='latin1')
data['tx'] = cPickle.load(open(prefix+'.tx', 'rb'), encoding='latin1').toarray()
data['ty'] = cPickle.load(open(prefix+'.ty', 'rb'), encoding='latin1')
graph_dict = cPickle.load(open(prefix+'.graph', 'rb'), encoding='latin1')

data['graph'] = graph.SimGraph(graph_dict)
data['graph'].makeWeight()
data['graph_mat'] = data['graph'].getAdjacencyMatrix()

