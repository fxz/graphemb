import os.path
import os
import numpy as np
import sys
from tensorflow.examples.tutorials.mnist import input_data
import dumper

_dir_name = os.path.dirname(__file__)

np.random.seed(42)

def write_data(k=3, dist2weight=None, lsize=500, trainsize=2000, testsize=5000):
    mnist = input_data.read_data_sets('MNIST_data', one_hot=True)
    prefix = os.path.join(_dir_name, 'mnist.{}.{}.{}'.format(lsize, trainsize, testsize))

    sys.path.append('../../')
    from graphemb.graph import SimGraph

    os.mkdir(prefix)
    idx = np.random.choice(mnist.train.num_examples, size=(trainsize,))
    tidx = np.random.choice(mnist.test.num_examples, size=(testsize,))
    mnist.train.graph = SimGraph()
    mnist.train.graph.loadData(np.vstack((mnist.train.images[idx, :], mnist.test.images[tidx, :])))
    mnist.train.graph.kNNConnect(k=3)
    mnist.train.graph.makeWeight(dist2weight=dist2weight)
    mnist.train.graph_mat = mnist.train.graph.getAdjacencyMatrix()

    basename = os.path.join(prefix, 'mnist')
    dumper.dump(mnist.train.images[idx, :],
                basename, ext='x')
    dumper.dump(mnist.train.labels[idx[:lsize], :],
                basename, ext='y')
    dumper.dump(mnist.train.graph, basename, ext='graph')
    dumper.dump(mnist.train.graph_mat,
                basename, ext='graph_mat')
    dumper.dump(mnist.test.images[tidx], basename, ext='tx')
    dumper.dump(mnist.test.labels[tidx], basename, ext='ty')

def make_dataset(lsize=500, trainsize=2000, testsize=5000):
    prefix = os.path.join(_dir_name, 'mnist.{}.{}.{}'.format(lsize, trainsize, testsize))
    if not os.path.isdir(prefix):
        write_data(k=3, dist2weight=None, lsize=lsize, trainsize=trainsize, testsize=testsize)

    data = {}
    data_prefix = os.path.join(prefix, 'mnist')
    data['x'] = dumper.load(data_prefix, ext='x')
    data['y'] = dumper.load(data_prefix, ext='y')
    data['tx'] = dumper.load(data_prefix, ext='tx')
    data['ty'] = dumper.load(data_prefix, ext='ty')
    data['graph'] = dumper.load(data_prefix, ext='graph')
    data['graph_mat'] = dumper.load(data_prefix, ext='graph_mat')
    return data

data = make_dataset()
