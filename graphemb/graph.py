#!/bin/env python

'''
The implementation for constructing a dissimilarity graph for the given data.
Baseline is the k-NN graph.
'''

# TODO: Make interface to adjacency matrix and spectral embeddings

import networkx as nx
import numpy as np
from sklearn.manifold import spectral_embedding

class SimGraph(nx.Graph):
    def __init__(self, *args):
        super().__init__(*args)
        self.data = np.empty(0)
        self._dist2weight = None

    def loadData(self, data):
        self.data = np.array(data)
        for i in range(self.data.shape[0]):
            self.add_node(i, data=self.data[i, :])

    def kNNConnect(self, k=3, ord=None):
        def getKnnIndex(feature, data, ord=ord, k=k+1):
            disp = data - np.tile(feature, data.shape[0]).reshape(data.shape)
            distance = np.linalg.norm(disp, ord=ord, axis=1)
            nearest_neighbors = np.argpartition(distance, k)[:k]
            return nearest_neighbors
        for n in self.nodes():
            nn_idx = getKnnIndex(self.node[n]["data"], self.data)
            if n in nn_idx:
                nn_idx = nn_idx[nn_idx != n]
            for i in nn_idx:
                self.add_edge(n, i, dist=np.linalg.norm(self.data[i] - self.data[n],
                                                        ord=ord))

    def setDist2Weight(self, func):
        self._dist2weight = func

    def makeWeight(self, dist2weight=None, key='weight'):
        if dist2weight is None:
            dist2weight = lambda d: np.exp(-d**2 / 2)
        for E in self.edges():
            if 'dist' in self.edges[E]:
                self.edges[E][key] = dist2weight(self.edges[E]["dist"])
            else:
                self.edges[E][key] = 1

    def getAdjacencyMatrix(self, **kwargs):
        return nx.adjacency_matrix(self, **kwargs)

    def getSpectEmbed(self, **kwargs):
        return spectral_embedding(nx.adjacency_matrix(self, weight='weight'),
                                  **kwargs)

