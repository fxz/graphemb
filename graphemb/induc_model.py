import tensorflow as tf
import numpy as np

import context_sampling

class InducModel():
    def __init__(self, args):
        self.x_hidden_node = args['x_hidden_node']
        self.e_hidden_node = args['e_hidden_node']
        self.lamb = args['lamb']
        self.N1 = args['N1'] # supervised sampling size
        self.N2 = args['N2'] # Sampling from all data batch size
        self.e_input_node = args['e_input_node']
        if 'supervised_rate' in args:
            self.supervised_rate = args['supervised_rate']
        else:
            self.supervised_rate = 0.1
        if 'unsupervised_rate' in args:
            self.unsupervised_rate = args['unsupervised_rate']
        else:
            self.unsupervised_rate = 0.01

        # defaults
        self.features = None
        self.labels = None
        self.supervised_loss = None
        self.unsupervised_loss = None
        self.x_input_node = 1
        self.y_output_node = 1
        self.graph = None
        self.num_data = 0

        # tf related
        self.sess = None
        self.embedding = None
        self.inputs = {}
        self.Ls_train_step = None

    def loadData(self, features, labels, graph):
        self.features = features
        self.labels = labels
        self.x_input_node = features.shape[1]
        self.y_output_node = labels.shape[1]
        self.graph = graph
        self.num_data = graph.shape[0]

    def buildNet(self):
        # Data for supervised training
        x = tf.placeholder(tf.float32, shape=[None, self.x_input_node])
        y_ = tf.placeholder(tf.float32, shape=[None, self.y_output_node])
        self.inputs['x'] = x
        self.inputs['y_'] = y_

        # Embedding for Supervised training

        def weight_variable(shape):
            initial = tf.truncated_normal(shape, stddev=0.1)
            return tf.Variable(initial)

        def bias_variable(shape):
            initial = tf.constant(0.1, shape=shape)
            return tf.Variable(initial)

        def initRandom(shape):
            initial = tf.random_normal(shape)
            return tf.Variable(initial)

        def forwardprop(X, w_x, b_x, w_xe, b_xe, w_e, b_e, w_2, b_2):
            h_x = tf.nn.relu(tf.matmul(X, w_x) + b_x)
            E = tf.nn.relu(tf.matmul(X, w_xe) + b_xe)
            h_e = tf.nn.relu(tf.matmul(E, w_e) + b_e)
            h = tf.concat([h_x, h_e], 1)
            y = tf.matmul(h, w_2) + b_2
            return y, E
        # Weights and bias for supervised neural network
        W_x = weight_variable([self.x_input_node, self.x_hidden_node])
        b_x = bias_variable([self.x_hidden_node])
        W_e = weight_variable([self.e_input_node, self.e_hidden_node])
        b_e = bias_variable([self.e_hidden_node])
        W_2 = weight_variable([self.x_hidden_node + self.e_hidden_node,
                               self.y_output_node])
        b_2 = bias_variable([self.y_output_node])

        # Layer between input and embedding
        W_xe = weight_variable([self.x_input_node, self.e_input_node])
        b_xe = bias_variable([self.e_input_node])

        # Embedding variables
        ufeature_i = tf.placeholder(tf.float32, shape=[None, self.x_input_node])
        ufeature_c = tf.placeholder(tf.float32, shape=[None, self.x_input_node])
        gamma = tf.placeholder(tf.int32)
        self.inputs['ufeature_i'] = ufeature_i
        self.inputs['ufeature_c'] = ufeature_c
        self.inputs['gamma'] = gamma

        # Embedding outputs
        E_i = tf.nn.relu(tf.matmul(ufeature_i, W_xe) + b_xe)
        W_c = tf.nn.relu(tf.matmul(ufeature_c, W_xe) + b_xe)

        # Forward Propagation Prediction
        y, e_u = forwardprop(x, W_x, b_x, W_xe, b_xe, W_e, b_e, W_2, b_2)
        self.embedding = e_u
        # Supervised Training
        self.Ls = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
        self.Ls_train_step = tf.train.GradientDescentOptimizer(0.5).minimize(self.Ls)
        # self.Lu = tf.reduce_mean(-tf.log(tf.nn.sigmoid(tf.multiply(tf.reduce_sum(           # method of tf.cast for gamma
        #     tf.multiply(W_c, E_i), axis=1), tf.cast(gamma, tf.float32)
        # ))))

        mul = tf.where(tf.equal(gamma, 1), tf.multiply(W_c, E_i), -tf.multiply(W_c, E_i))     # method of tf.where for gamma
        self.Lu = tf.reduce_mean(-tf.log(tf.nn.sigmoid(tf.reduce_sum(mul, axis=1))))

        self.Lu_train_step = tf.train.GradientDescentOptimizer(0.01).minimize(self.Lu)

        # build prediction logics
        correct_prediction = tf.equal(tf.argmax(y_, axis=1), tf.argmax(y, axis=1))
        self.accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    def init_train(self):
        self.sess = tf.Session()
        return self.sess

    def pre_train(self, t1=2000, t2=100, N1=None, N2=None, max_iter=5):
        for i in range(max_iter):
            Ls, Lu = self.train_step(t1=t1, t2=t2, N1=N1, N2=N2)
            print("pre-training step ", i)
        print("Pretraining finished, Ls = ", Ls, "Lu = ", Lu)

    def reset_train(self):
        if self.sess is not None:
            self.sess.close()
            self.sess = None

    def train_step(self, t1, t2, n1, n2):
        if (self.sess is None):
            raise ValueError("@@ call init_train first to start a tf session")
        # supervised
        label_sample = context_sampling.LabeledBatch(self.labels, n1)
        # unsupervised
        context_sample = context_sampling.ContextBatch(np.argmax(self.labels, axis=1),
                                                       self.graph.toarray())
        Ls_list = np.zeros(t1)
        Lu_list = np.zeros(t2)

        self.sess.run(tf.global_variables_initializer())
        for i in range(t1):
            labeled_batch = next(label_sample)
            self.sess.run(self.Ls_train_step,
                          feed_dict={self.inputs['x']: self.features[labeled_batch, :],
                                     self.inputs['y_']: self.labels[labeled_batch, :]})
            Ls_value = self.sess.run(self.Ls,
                                    feed_dict={self.inputs['x']: self.features[labeled_batch, :],
                                               self.inputs['y_']: self.labels[labeled_batch, :]})
            Ls_list[i] = Ls_value

        for i in range(t2):
            unlabeled_batch = next(context_sample)
            self.sess.run(self.Lu_train_step,
                          feed_dict={self.inputs['ufeature_i']:
                                     self.features[unlabeled_batch[:, 0], :],
                                     self.inputs['ufeature_c']:
                                     self.features[unlabeled_batch[:, 1], :],
                                     self.inputs['gamma']: unlabeled_batch[:, 2]})

            Lu_value = self.sess.run(self.Lu,
                          feed_dict={self.inputs['ufeature_i']:
                                     self.features[unlabeled_batch[:, 0], :],
                                     self.inputs['ufeature_c']:
                                     self.features[unlabeled_batch[:, 1], :],
                                     self.inputs['gamma']: unlabeled_batch[:, 2]})
            Lu_list[i] = Lu_value

        return np.mean(Ls_list), np.mean(Lu_list)

    def getAccuracy(self, tx, ty, index):
        accuracy = self.sess.run(self.accuracy,
                                 feed_dict={self.inputs['x']: tx,
                                            self.inputs['y_']: ty})
        return accuracy
