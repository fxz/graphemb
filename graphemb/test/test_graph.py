#!/bin/env python
# -*- coding: utf-8 -*-
'''
Test cases for the kNN graph
'''

import unittest
import sys
import numpy as np

sys.path.append("../")

import graph

class TestGraph(unittest.TestCase):
    def setUp(self):
        self.G = graph.SimGraph()

    def testConnect(self):
        data = np.array([
            [1, 1],
            [1, -1],
            [-1, -1],
            [-1, 1],
            [0, 0]
        ])
        self.G.loadData(data)
        self.G.kNNConnect(k=1)
        for i in range(4):
            self.assertTrue((i, 4) in self.G.edges())

    def testMatrix(self):
        data = np.array([
            [1, 1],
            [1, -1],
            [-1, -1],
            [-1, 1],
            [0, 0]
        ])
        self.G.loadData(data)
        self.G.kNNConnect(k=1)
        self.G.makeWeight() # default is KBF, with the key "weight"
        adj_mat = self.G.getAdjacencyMatrix(weight='weight')
        dist = np.exp(- 1)
        for i in range(4):
            self.assertTrue(np.abs(dist - adj_mat[i, 4]) < 1.e-8)
            self.assertTrue(np.abs(dist - adj_mat[4, i]) < 1.e-8)


if __name__ == "__main__":
    unittest.main()
