import sys
import unittest
import numpy as np

sys.path.append('../')

import context_sampling
import dataset.mnist
import dataset.citeseer

class TestSampling(unittest.TestCase):
    def testLabelBatch(self):
        data = dataset.mnist.make_dataset(lsize=2000, trainsize=2000,
                                          testsize=5000)
        sample = context_sampling.LabeledBatch(data['y'], 10)
        for _ in range(500):
            batch = next(sample)
            self.assertEqual(batch.shape[0], 10)

        data = dataset.citeseer.data
        sample = context_sampling.LabeledBatch(data['y'], 10)
        for _ in range(500):
            batch = next(sample)
            self.assertEqual(batch.shape[0], 10)

    @unittest.skip('ignore')
    def testContextBatch(self):
        sampling = context_sampling.ContextBatch(np.argmax(dataset.mnist.data['y'][:500],
                                                           axis=1),
                                                 dataset.mnist.data['graph_mat'].toarray(),
                                                 r2=0.9, N2=10)
        for _ in range(10):
            print(next(sampling))

if __name__ == "__main__":
    unittest.main()
