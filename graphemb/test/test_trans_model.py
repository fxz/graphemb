import unittest
import sys
import numpy as np

sys.path.append('../')

import trans_model
import context_sampling
import dataset.mnist

class TestTransModel(unittest.TestCase):
    def setUp(self):
        self.args = {'x_hidden_node': 200,
                     'e_hidden_node': 10,
                     'e_input_node': 10,
                     'lamb': 1,
                     'N1': 50,
                     'N2': 50,
                     't1': 10,
                     't2': 10,
                     'total_size': dataset.mnist.data['graph_mat'].shape[0]}
        self.model = trans_model.TransModel(self.args)

    def tearDown(self):
        self.model.reset_train()

    def testLoadData(self):
        self.model.loadData(dataset.mnist.data['x'], dataset.mnist.data['y'], dataset.mnist.data['graph_mat'])

    def testBuildNet(self):
        self.model.buildNet()
        inputs = ['x', 'y_', 'e_s', 'c', 'i', 'gamma']
        for key in inputs:
            self.assertTrue(key in self.model.inputs)

    @unittest.skip('Passed, also tested in testAccuracy')
    def testTrain(self):
        self.model.loadData(dataset.mnist.data['x'], dataset.mnist.data['y'],
                            dataset.mnist.data['graph_mat'])
        self.model.buildNet()
        self.model.init_train()
        for _ in range(10):
            Ls, Lu = self.model.train_step(self.args['t1'], self.args['t2'])
            print('Loss: ', Ls + self.model.lamb * Lu)

        self.model.reset_train()

    def testAccuracy(self):
        self.model.loadData(dataset.mnist.data['x'], dataset.mnist.data['y'],
                            dataset.mnist.data['graph_mat'])
        self.model.buildNet()
        self.model.init_train()
        for _ in range(50):
            Ls, Lu = self.model.train_step(self.args['t1'], self.args['t2'])
            print('Ls: ', Ls, 'Lu: ', Lu)

        trainsize = dataset.mnist.data['x'].shape[0]
        testsize = dataset.mnist.data['tx'].shape[0]
        print(self.model.getAccuracy(dataset.mnist.data['tx'], dataset.mnist.data['ty'],
                                  np.arange(trainsize, trainsize+testsize)))

        self.model.reset_train()

class TestInducModel(unittest.TestCase):
    def setUp(self):
        self.args = {'x_hidden_node': 200,
                     'e_hidden_node': 10,
                     'e_input_node': 10,
                     'lamb': 1,
                     'N1': 10,
                     'N2': 10,
                     't1': 10,
                     't2': 10,
                     'total_size': dataset.mnist.data['graph_mat'].shape[0]}
        self.model = trans_model.TransModel(self.args)

    def tearDown(self):
        self.model.reset_train()

    def testLoadData(self):
        self.model.loadData(dataset.mnist.data['x'], dataset.mnist.data['y'], dataset.mnist.data['graph_mat'])

    def testBuildNet(self):
        self.model.buildNet()
        inputs = ['x', 'y_', 'e_s', 'c', 'i', 'gamma']
        for key in inputs:
            self.assertTrue(key in self.model.inputs)

    @unittest.skip('Passed, also tested in testAccuracy')
    def testTrain(self):
        self.model.loadData(dataset.mnist.data['x'], dataset.mnist.data['y'],
                            dataset.mnist.data['graph_mat'])
        self.model.buildNet()
        self.model.init_train()
        for _ in range(10):
            Ls, Lu = self.model.train_step(self.args['t1'], self.args['t2'],
                                           self.args['N1'], self.args['N2'])
            print('Loss: ', Ls + self.model.lamb * Lu)

        self.model.reset_train()

    def testAccuracy(self):
        self.model.loadData(dataset.mnist.data['x'], dataset.mnist.data['y'],
                            dataset.mnist.data['graph_mat'])
        self.model.buildNet()
        self.model.init_train()
        for _ in range(10):
            Ls, Lu = self.model.train_step(self.args['t1'], self.args['t2'],
                                           self.args['N1'], self.args['N2'])
            print('Loss: ', Ls + self.model.lamb * Lu)

        trainsize = dataset.mnist.data['x'].shape[0]
        testsize = dataset.mnist.data['tx'].shape[0]
        print(self.model.getAccuracy(dataset.mnist.data['tx'], dataset.mnist.data['ty'],
                                  np.arange(trainsize, trainsize+testsize)))

        self.model.reset_train()

if __name__ == '__main__':
    unittest.main()
