#!/bin/env python

import sys
import numpy as np
import json
import argparse
import os

sys.path.append('../')

import trans_model
import graph
import context_sampling

parser = argparse.ArgumentParser(description='The transductive graph embedding semisupervised classifier example')

parser.add_argument('--config', help='Specify the json config file', type=str)
args = parser.parse_args()

PRETRAINED = os.path.join(os.path.dirname(os.path.abspath(__file__)), "pretrained.ckpt")
print("pretrained path is", PRETRAINED)

def run_model(data, conf, output='test-{}.log'.format(args.config)):
    args = conf['model']
    args['total_size'] = data['graph_mat'].shape[0]
    trainsize = data['x'].shape[0]
    testsize = data['tx'].shape[0]

    model = trans_model.TransModel(args)
    model.loadData(data['x'], data['y'], data['graph_mat'])
    model.buildNet()
    model.init_train()
    if os.path.isfile(PRETRAINED + 'index'):
        print('Pre-trained session exists, loading...')
        model.load(PRETRAINED)
    else:
        print('Start pre-training')
        model.pre_train(t1=100, t2=1000, N1=100, N2=200, max_iter=1)
        print('Pre-training finished')
        accuracy = model.getAccuracy(data['tx'], data['ty'],
                                    np.arange(trainsize, trainsize+testsize))
        print('Pre-trained accuracy: ', accuracy)
        model.save(PRETRAINED)

    sampling_conf = {
        'r1': conf['model']['r1'],
        'r2': conf['model']['r2'],
        'q': conf['model']['q'],
        'd': conf['model']['d']
    }
    last_acc = 0
    with open(output, 'a') as f:
        f.write("========== Test Begins ==========\n")
        f.write("## The config is\n")
        f.write(json.dumps(conf, indent=4, sort_keys=True))
        f.write("\n")
        f.write("## ============= Logs follow ===============\n")

    print('Start Training')
    if conf['model']['train_step'] is not 0:
        for i in range(conf['model']['train_step']):
            Ls, Lu = model.train_step(conf['model']['t1'], conf['model']['t2'], **sampling_conf)
            total_loss = Ls + conf['model']['lamb'] * Lu
            accuracy = model.getAccuracy(data['tx'], data['ty'],
                                        np.arange(trainsize, trainsize+testsize))
            if accuracy > last_acc:
                last_acc = accuracy
                with open(output, 'a') as f:
                    f.write("Ls: {}, Lu: {}, total loss: {}\tAccuracy: {}\n".format(Ls,
                                                                                    Lu,
                                                                                    total_loss,
                                                                                    accuracy))
            if i % 10 == 0:
                accuracy = model.getAccuracy(data['tx'], data['ty'],
                                        np.arange(trainsize, trainsize+testsize))
                print("Ls: {}, Lu: {}, total loss: {}\tAccuracy: {}".format(Ls,
                                                                                Lu,
                                                                                total_loss,
                                                                                accuracy))
        model.reset_train()
        f.write("========== Test Ends ==========\n")
    else:
        nstep = 0
        while True:
            Ls, Lu = model.train_step(conf['model']['t1'], conf['model']['t2'], **sampling_conf)
            total_loss = Ls + conf['model']['lamb'] * Lu
            accuracy = model.getAccuracy(data['tx'], data['ty'],
                                    np.arange(trainsize, trainsize+testsize))
            if accuracy > last_acc:
                with open(output, 'a') as f:
                    f.write("Ls: {}, Lu: {}, total loss: {}\tAccuracy: {}\n".format(Ls,
                                                                                Lu,
                                                                                total_loss,
                                                                                accuracy))
                last_acc = accuracy
                if not os.path.isdir("best_result"):
                    os.mkdir("best_result")
                model.save(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                        "best_result/best"))
            if nstep % 10 == 0:
                accuracy = model.getAccuracy(data['tx'], data['ty'],
                                        np.arange(trainsize, trainsize+testsize))
                print("Ls: {}, Lu: {}, total loss: {}\tAccuracy: {}".format(Ls,
                                                                                Lu,
                                                                                total_loss,
                                                                                accuracy))
            nstep = nstep + 1


def main():
    conf = json.load(open(args.config, 'r'))
    if conf['data']['name'] == 'mnist':
        sys.path.append('../')
        import dataset.mnist
        data = dataset.mnist.make_dataset(lsize=conf['data']['trainsize'],
                                          trainsize=conf['data']['trainsize'],
                                          testsize=conf['data']['testsize'])
        run_model(data, conf)
    elif conf['data']['name'] == 'citeseer':
        sys.path.append('../')
        import dataset.citeseer
        data = dataset.citeseer.data
        run_model(data, conf)

if __name__ == '__main__':
    main()
